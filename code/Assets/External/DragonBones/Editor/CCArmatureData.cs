﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditorInternal;

using System;
using System.IO;
using DragonBones;
using DragonBones.Factorys;
using DragonBones.Animation;
using DragonBones.Objects;
using DragonBones.Display;
using DragonBones.Textures;
using DragonBones.Utils;
using System.Collections.Generic;
using Com.Viperstudio.Utils;
using Com.Viperstudio.Geom;

public class CCArmatureData  : CCArmatureDataBase{

	private TextureAtlas thisTextureAtlas = null;
	private SkeletonData thisSkeletonData = null;
	/// <summary>
	///  龙骨数据目录
	/// </summary>
	protected string m_DBPath;
	/// <summary>
	///  龙骨图片的目录
	/// 	如果存在则是m_IsMultiplePNGs
	/// </summary>
	private string m_TextureDir;
	/// <summary>
	///  每个龙骨数据文件的路径
	/// </summary>
	private string m_TexturePath;
	private string m_TextureJsonPath;
	private string m_SkeletonJsonPath;
	/// <summary>
	/// 是否是多图片
	/// 	否则是单图合集
	/// </summary>
	private bool m_IsMultiplePNGs = false;
	/// <summary>
	/// 保存图集信息
	/// </summary>
	private Dictionary<String, System.Object> m_TextureInfo = null;

	/// <summary>
	/// 构造函数
	/// </summary>
	public CCArmatureData () {
		string path = getSelectPath();
		
		m_DBPath = path;
		m_TextureDir = path + "texture/";
		m_TexturePath = path + "texture.png";
		m_TextureJsonPath = path + "texture.json";
		m_SkeletonJsonPath = path + "skeleton.json";
		
		thisTextureAtlas = getTextureAtlaDataFrom(m_TexturePath, m_TextureJsonPath, ref m_TextureInfo);
		thisSkeletonData = getSkeletonDataFrom(m_SkeletonJsonPath);

		if (Directory.Exists(m_TextureDir)) {
			Debug.Log("这是texture目录,多个PNGS");
			m_IsMultiplePNGs = true;
		}
	}

	/// <summary>
	///  龙骨数据转换unity数据
	/// 	1 如果是图集，裁剪SpriteSheet
	/// 		是多图片，Texture文件夹每个图片转换Sprite
	/// 	2 取出Sprite列表
	/// </summary>
	public void DragonBoneToUnity () {
		if (!m_IsMultiplePNGs) {
			CCAramtureImportHelp.BuildSpriteDataFromAtals (m_TexturePath, m_TextureInfo, thisTextureAtlas);
		} else {
			//系统自动转化就好!!!
		}

		BuildUnityAssetData();
	}


	/// <summary>
	///  龙骨中全部Display的锚点偏移
	/// 	构建骨骼节点时用
	/// </summary>
	Dictionary<string, Vector2> m_AllDisplayPovit;
	/// <summary>
	///  取出Sprite保存
	/// </summary>
	Dictionary<string, Sprite> m_AllSprite;
	/// <summary>
	/// 取出SlotData临时数据
	/// </summary>
	Dictionary<string, SlotData> m_listSlotData;
	/// <summary>
	/// 	临时数据
	/// 	m_Prefab		正在构建的Prefab
	/// 	m_BoneTreeRoot  Prefab的根Transform
	/// 	m_listNodes		Prefab的Children的Transform
	/// 	动画状态机
	/// 	m_Layer			状态机中的层
	/// 	m_States		状态机的所有状态
	/// 	m_NewClip		新的动画剪辑
	/// </summary>
	UnityEngine.Object m_Prefab = null;
	Transform m_BoneTreeRoot;
	Dictionary<string, Transform> m_listChildren;
	AnimatorControllerLayer m_Layer;
	List<State> m_States;
	AnimationClip m_NewClip;


	/// <summary>
	///  生成Unity数据
	/// 	1 Prefab 保存在当前数据下，name是ArmatureData.prefab
	/// 		更新的时候要保证原来的root的组件还存在.
	/// 		保证关联还存在
	/// 		主要是TransformChild
	/// 			Child的name是图层: name是注意要保证唯一
	/// 	2 Animator 名字是ArmatureData.animator
	/// 		里面的state的name用的是AnimationData,更新不改变
	/// 		如果state没有关联clip则添加，否则更新name中的关联
	/// 	3 AnmationClip 名字是ArmatureData@AnimationData.anim
	/// 		文件,不做添加动画事件的添加
	/// 		各种曲线 (序列帧时的常量曲线)
	/// 		Sprite的序列
	/// 		Color
	/// </summary>
	void BuildUnityAssetData () 
	{
		m_AllDisplayPovit = IteratesAllDisplayPovit(thisSkeletonData);
		m_AllSprite = m_IsMultiplePNGs ? CCAramtureImportHelp.getSpriteListFromTextureDic(m_TextureDir) :
			CCAramtureImportHelp.getSpriteListFromDic(m_TexturePath);

		SkeletonData skeletonData = thisSkeletonData;
		foreach (ArmatureData data in skeletonData.armatureDataList) {
			Debug.Log ("======================");
			Debug.Log ("ArmatureName >= " + data.Name);

			CheckPrefabStorage (data.Name);
			m_listChildren = CCAramtureImportHelp.BuildBoneTree (m_BoneTreeRoot.gameObject, data, m_AllSprite);
			m_listSlotData = CCAramtureImportHelp.BuildBoneSprite(data,m_listChildren,m_AllSprite,m_AllDisplayPovit);

			m_States = CCAramtureImportHelp.BuildStateMachine(m_BoneTreeRoot.gameObject, out m_Layer);

			foreach (AnimationData animationData in data.AnimationDataList) {

				string clipName = m_BoneTreeRoot.gameObject.name + "@" + animationData.Name;
				string stateName= CCArmatureIO.getStandardName (animationData.Name);

				m_NewClip = CCAramtureImportHelp.BuildAnimationClipAndState (clipName, stateName, m_States, m_Layer);
				m_NewClip.ClearCurves();	

				foreach (KeyValuePair<string,TransformTimeline> timeLine in animationData.Timelines) {

					List<Frame> frames = timeLine.Value.FrameList;					
					FrameTransposition mFrameTransposition = new FrameTransposition(frames.Count);
					List<DisplayData> mDisdata = m_listSlotData [timeLine.Key].DisplayDataList;					
					CollectSpriteFrame mCollectSpriteFrame;
					mCollectSpriteFrame= new CollectSpriteFrame(m_AllSprite,m_AllDisplayPovit,mDisdata,data.Name,timeLine.Key);
					ColorSpriteFrame mColorFrame = new ColorSpriteFrame();
					HideSpriteFrame mHide = new HideSpriteFrame(frames.Count);	
					
					float z = m_listChildren [timeLine.Key].localPosition.z;
					float m_time = 0;
					float pre_Time = 0;

					// TODO : 检查合格的帧数据
					if (frames != null && frames.Count < 2) {
						if (frames.Count == 1)						
							Debug.LogWarning(data.Name + " + " + animationData.Name + " + " + timeLine.Key + " 少于2帧");
					}

					if (frames != null && frames.Count > 1) {
						string panelName = CCAramtureImportHelp. getPathFromTree(timeLine.Key, m_listChildren[timeLine.Key]);
						
						for (int i=0; i<frames.Count; ++i) {
							TransformFrame pre_Frame = null;
							TransformFrame tframe = frames [i] as TransformFrame;
							if (i > 0)
								pre_Frame = frames [i - 1] as TransformFrame;
							
							if (tframe == null) {
								Debug.Log("Hello , Frame isnot TransformFrame >= " + timeLine.Key + " " + i);
								continue;
							}
							do {
								TransformFrame theFrame = tframe;
								if (i > 0 && theFrame.DisplayIndex < 0)
									theFrame = frames [i - 1] as TransformFrame;
								float ox = timeLine.Value.OriginPivot.X;
								float oy = timeLine.Value.OriginPivot.Y;
								mFrameTransposition.AddFrame(i, m_time, theFrame, z, ox, oy);
							} while (false);
							do {
								if ((pre_Frame == null && tframe.DisplayIndex > 0) ||
								    (pre_Frame != null && pre_Frame.DisplayIndex != tframe.DisplayIndex && tframe.DisplayIndex >= 0)) {
									mCollectSpriteFrame.AddKey(m_time, tframe.DisplayIndex);
								}
							} while (false);
							do {
								if (tframe.Color != null && pre_Frame != null && pre_Frame.Color == null) {
									mColorFrame.AddKey(pre_Time, mColorFrame.getColor());
								}
								if (pre_Frame != null && pre_Frame.Color != null && tframe.Color == null) {
									mColorFrame.AddKey(m_time, mColorFrame.getColor());
								}
								mColorFrame.AddKey(m_time, tframe.Color);
							} while (false);
							do {
								if (tframe.DisplayIndex < 0 && pre_Frame != null && pre_Frame.DisplayIndex >= 0) {
									mHide.AddKey(pre_Time, i - 1, pre_Frame.DisplayIndex);								
									mHide.AddKey(m_time, i, tframe.DisplayIndex);
								} else if (tframe.DisplayIndex < 0 && pre_Frame == null) {
									mHide.AddKey(m_time, i, tframe.DisplayIndex);
								} else if (tframe.DisplayIndex >= 0 && pre_Frame != null && pre_Frame.DisplayIndex < 0) {
									mHide.AddKey(pre_Time, i - 1, pre_Frame.DisplayIndex);								
									mHide.AddKey(m_time, i, tframe.DisplayIndex);
								}
							} while (false);
							
							if (i < frames.Count - 1) {
								pre_Time = m_time;
								m_time += tframe.Duration;
							}
						}
						
						mFrameTransposition.PushTo(m_NewClip, panelName, m_time);
						string aboutSlot = panelName + "/slot@" + timeLine.Key;
						mCollectSpriteFrame.SetObjectReferenceCurve(m_NewClip, aboutSlot);
						mColorFrame.SetObjectReferenceCurve(m_NewClip, aboutSlot);
						mHide.SetObjectReferenceCurve(m_NewClip, panelName);
					}
				}
				CCAramtureImportHelp. AddHideTimeline(m_NewClip, animationData.Duration, m_listChildren, animationData, data);
				m_NewClip.frameRate = 60;
			}
			CheckPrefabStoragetConenctPrefab(m_BoneTreeRoot.gameObject);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
	}


	/// <summary>
	/// 遍历SkeletonData中DisplayData
	/// </summary>
	static Dictionary<string, Vector2> IteratesAllDisplayPovit(SkeletonData skeletonData)
	{
		Dictionary<string, Vector2> pool = new Dictionary<string, Vector2> ();

		foreach (ArmatureData data in skeletonData.armatureDataList) {
			foreach (SkinData skinData in data.SkinDataList) {
				foreach (SlotData slotData in skinData.SlotDataList) {
					MSG(slotData.DisplayDataList != null && slotData.DisplayDataList.Count > 0);
					foreach (DisplayData displayData in slotData.DisplayDataList) {
						string key = data.Name + slotData.Name + displayData.Name;
						key = key.Replace('/', '@');
						pool.Add(key, new Vector2(-(displayData.Pivot.X - displayData.Transform.X),
						                                       displayData.Pivot.Y - displayData.Transform.Y));
					}
				}
			}
		}		
		return pool;
	}

	/// <summary>
	/// 检查存储的Prefab
	/// 	如果存在Prefab则返回Prefab的实例
	/// 	否则新建 
	/// </summary>
	/// <returns>The prefab storage.</returns>
	GameObject CheckPrefabStorage (string name) {
		GameObject g;
		string path = CCArmatureIO .getAnimGamePath(name);
		if (File.Exists(path)) {
			Debug.Log("### PREFABS >= " + path);
			m_Prefab = AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));
			g = PrefabUtility.InstantiatePrefab(m_Prefab) as GameObject;
		} else {
			Debug.Log("### NO PREFAB ... ...");
			g = new GameObject(CCArmatureIO.getStandardName(name));
			m_Prefab = null;
		}
		m_BoneTreeRoot = g.transform;
		return g;
	}

	/// <summary>
	/// 检查Prefab的链接
	/// </summary>
	/// <param name="g">The green component.</param>
	void CheckPrefabStoragetConenctPrefab(GameObject g)
	{
		UnityEngine.Object temp = m_Prefab;
		if (temp == null) {
			CCArmatureIO.CreatePrefab(g, g.name);
		} else {
			PrefabUtility.ReplacePrefab(g, m_Prefab, ReplacePrefabOptions.ConnectToPrefab);
		}
	}
}
