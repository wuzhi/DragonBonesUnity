﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;

using System;
using System.IO;

using LitJson;

public class CCDragonSprite {

	string[] m_file_Extension = new string[] {
		".png", ".jpg"
	};

	public void Exec () {
		UnityEngine.Object select = Selection.activeObject;
		string path = AssetDatabase.GetAssetPath (select);

		string m_ext = Path.GetExtension (path);
		foreach (string ext in m_file_Extension) {
			if (m_ext == ext) {

				Debug.Log ("###### 裁剪精灵 ######");
				Debug.Log ("path: " + path);
				Debug.Log ("name: " + select.name);

				m_TexturePath = path;
				m_JsonPath = Path.GetDirectoryName (path) + @"/"+ select.name + @".json";
				ReBuildSpriteData ();
				break;
			}
		}
	}

	string m_TexturePath;
	string m_JsonPath;
	FlashToJsonCls m_FlashMeta;

	void ReBuildSpriteData()
	{
		TextAsset textJson = Resources.LoadAssetAtPath <TextAsset> (m_JsonPath) as TextAsset;
		if (textJson == null) {
			throw new Exception ("没有Json文件信息");
		}
		m_FlashMeta = JsonMapper.ToObject<FlashToJsonCls> (textJson.ToString()) as FlashToJsonCls;

		TextureImporter texImporter = GetTextureSettings01(m_TexturePath);
		texImporter.mipmapEnabled = false;
		TextureImporterSettings tis = new TextureImporterSettings();
		texImporter.ReadTextureSettings(tis);
		tis.spriteMeshType = SpriteMeshType.FullRect;
		texImporter.SetTextureSettings(tis);
		AssetDatabase.ImportAsset(m_TexturePath);
	}
	
	TextureImporter GetTextureSettings01(string path)
	{
		TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		textureImporter.textureType = TextureImporterType.Sprite;
		textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
		textureImporter.spriteImportMode = SpriteImportMode.Multiple;
		textureImporter.spritesheet = CutSpriteSheetFromInfo01();
		textureImporter.maxTextureSize = Mathf.Max (m_FlashMeta.meta.size.w , m_FlashMeta.meta.size.h);
		
		return textureImporter;
	}

	SpriteMetaData[] CutSpriteSheetFromInfo01() {
		int length = m_FlashMeta.frames.Count;
		SpriteMetaData[] datas = new SpriteMetaData[length];
		int h = m_FlashMeta.meta.size.h;
		for (int i=0; i< length; ++i ){
			FlashSprite s = m_FlashMeta.frames[i];

			SpriteMetaData sprite = new SpriteMetaData();
			sprite.name = s.filename;
			sprite.border = new Vector4(0, 0, 0, 0);
			sprite.rect = new Rect(s.frame.x, h-s.frame.y-s.frame.h, s.frame.w, s.frame.h);
			sprite.alignment = 0;
			sprite.pivot = Vector2.one * 0.5f;

			datas[i] = sprite;
		}
		return datas;
	}

	[System.Serializable]
	public class FlashToJsonCls {
		public List<FlashSprite> frames = new List<FlashSprite>();
		public FlashMeta meta;
	}

	[System.Serializable]
	public class FlashSprite {
		public string filename;
		public SpriteFrame frame;
		public bool rotated;
		public bool trimmed;
		public SpriteFrame spriteSourceSize;
		public FrameWidthHeight sourceSize;
	}

	[System.Serializable]
	public class SpriteFrame {
		public int x;
		public int y;
		public int w;
		public int h;
	}

	[System.Serializable]
	public class FrameWidthHeight {
		public int w;
		public int h;
	}

	[System.Serializable]
	public class FlashMeta {
		public string app;
		public string version;
		public string image;
		public string format;
		public FrameWidthHeight size;
		public string scale;
	}
}
