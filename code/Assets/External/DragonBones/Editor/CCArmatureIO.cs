﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using UnityEditorInternal;


public static class CCArmatureIO {

	/// <summary>
	///  动画控制器的路径
	/// </summary>
	/// <returns>The animator path.</returns>
	/// <param name="name">Name.</param>
	public static string getAnimatorPath(string name)
	{
		name = getStandardName(name);
		//return "Assets/Animations/" + name + ".controller";
		return SelectPath + name + ".controller";
	}

	/// <summary>
	/// 动画剪辑的路径
	/// </summary>
	/// <returns>The animation clip path.</returns>
	/// <param name="name">Name.</param>
	public static string getAnimClipPath(string name)
	{
		name = getStandardName(name);
		//return "Assets/Animations/" + name.Trim () + ".anim";
		return SelectPath + name.Trim () + ".anim";
	}

	/// <summary>
	/// Gets the name of the standard.
	/// </summary>
	/// <returns>The standard name.</returns>
	/// <param name="name">Name.</param>
	public static string getStandardName (string name) {
		return name.Replace ('/', '@');
	}

	/// <summary>
	/// Gets the animation game path.
	/// </summary>
	/// <returns>The animation game path.</returns>
	/// <param name="name">Name.</param>
	public static string getAnimGamePath(string name)
	{
		name = getStandardName(name);
		//return "Assets/Prefabs/" + name + ".prefab";
		return SelectPath + name + ".prefab";
	}

	/// <summary>
	/// Checks the out save directory.
	/// </summary>
	static void CheckOutSaveDirectory()
	{
		CheckOutDirectory("Assets", "Animations");
		CheckOutDirectory("Assets", "Prefabs");
		AssetDatabase.Refresh();
	}

	/// <summary>
	/// Checks the out directory.
	/// </summary>
	/// <param name="ppath">Ppath.</param>
	/// <param name="path">Path.</param>
	static void CheckOutDirectory(string ppath, string path)
	{
		if (!Directory.Exists(ppath + "/" + path)) {
			AssetDatabase.CreateFolder(ppath, path);
		}
	}

	/// <summary>
	/// Gets the name of the sprite.
	/// </summary>
	/// <returns>The sprite name.</returns>
	/// <param name="armatureName">Armature name.</param>
	/// <param name="display">Display.</param>
	/// <param name="slot">Slot.</param>
	public static string getSpriteName(string armatureName, string display, string slot)
	{
		return getSpriteName(armatureName, display, slot, 0, 0);
	}

	/// <summary>
	/// Gets the name of the sprite.
	/// </summary>
	/// <returns>The sprite name.</returns>
	/// <param name="armatureName">Armature name.</param>
	/// <param name="display">Display.</param>
	/// <param name="slot">Slot.</param>
	/// <param name="px">Px.</param>
	/// <param name="py">Py.</param>
	public static string getSpriteName(string armatureName, string display, string slot, int px, int py)
	{
		//string name = armatureName + "@" + display + "@" + slot;//+ "@" + px.ToString() + "_" + py.ToString();
		//name = name.Replace('/', '@').GetHashCode ().ToString ();
		//return name;
		return display.Replace ('/', '@');
	}

	/// <summary>
	/// Creates the animation clip for animator.
	/// </summary>
	/// <returns>The animation clip for animator.</returns>
	/// <param name="file">File.</param>
	public static string CreateAnimationClipForAnimator(string file)
	{
		AnimationClip clip = AnimatorController.AllocateAnimatorClip(file);
		if (!AssetDatabase.Contains(clip)) {
			AssetDatabase.CreateAsset(clip, file);
		}
		return file;
	}

	/// <summary>
	/// Creates the prefab.
	/// </summary>
	/// <returns>The prefab.</returns>
	/// <param name="go">Go.</param>
	/// <param name="name">Name.</param>
	public static UnityEngine.Object CreatePrefab(GameObject go, string name)
	{
		string path = getAnimGamePath(name);
		UnityEngine.Object tempPrefab = PrefabUtility.CreateEmptyPrefab(path);
		tempPrefab = PrefabUtility.ReplacePrefab(go, tempPrefab, ReplacePrefabOptions.ConnectToPrefab);
		return tempPrefab;
	}

	/// <summary>
	/// Gets the select path.
	/// </summary>
	/// <value>The select path.</value>
	public static string SelectPath {
		get {
			return CCArmatureDataBase.getSelectPath ();
		}
	}
}
