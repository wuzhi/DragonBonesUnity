﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditorInternal;

using System;
using System.IO;
using DragonBones;
using DragonBones.Factorys;
using DragonBones.Animation;
using DragonBones.Objects;
using DragonBones.Display;
using DragonBones.Textures;
using DragonBones.Utils;
using System.Collections.Generic;
using Com.Viperstudio.Utils;
using Com.Viperstudio.Geom;

public static class CCAramtureImportHelp {

	/// ============================================================================
	/// ============================================================================
	/// 拿出Sprite

	/// <summary>
	/// 从一个Texture中取Sprite
	/// </summary>
	public static Dictionary<string, Sprite> getSpriteListFromDic(string path)
	{
		Dictionary<string, Sprite> mlistSprite = new Dictionary<string, Sprite>();
		UnityEngine.Object[] ts = AssetDatabase.LoadAllAssetsAtPath(path);
		foreach (UnityEngine.Object t in ts) {
			if (t as Sprite) {
				mlistSprite.Add(t.name, t as Sprite);
			}
		}
		return mlistSprite;
	}

	/// <summary>
	/// 从一个目录中取Sprite
	/// 	1 先获取Png
	///  	2 从Png获取是否是Sprite
	/// </summary>
	public static Dictionary<string, Sprite> getSpriteListFromTextureDic(string path)
	{
		Dictionary<string, Sprite> mlistSprite = new Dictionary<string, Sprite>();
		string[] files = Directory.GetFiles(path); 
		foreach (string p in files) {
			if (".png" == Path.GetExtension(p)) {
				ReImportPNGToSprite(p);
				UnityEngine.Object[] ts = AssetDatabase.LoadAllAssetsAtPath(p);
				foreach (UnityEngine.Object t in ts) {
					if (t as Sprite) {
						mlistSprite.Add(t.name, t as Sprite);
					}
				}
			}
		}
		return mlistSprite;
	}

	static void ReImportPNGToSprite (string path) {
		TextureImporter texImporter = GetTextureSettings(path);
		texImporter.mipmapEnabled = false;
		TextureImporterSettings tis = new TextureImporterSettings();
		texImporter.ReadTextureSettings(tis);
		tis.spriteMeshType = SpriteMeshType.FullRect;
		tis.spriteAlignment = 1;
		texImporter.ClearPlatformTextureSettings ("iPhone");
		texImporter.SetTextureSettings(tis);
		AssetDatabase.ImportAsset(path);
	}

	static TextureImporter GetTextureSettings(string path)
	{
		TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		textureImporter.textureType = TextureImporterType.Sprite;
		textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
		return textureImporter;
	}

	/// ============================================================================
	/// ============================================================================
	/// 裁剪或转化Sprite


	/// <summary>
	/// 从合集裁剪精灵
	/// </summary>
	public static void BuildSpriteDataFromAtals(string path,
	                                       Dictionary<string, object> textureInfo,
	                                       TextureAtlas atlas)
	{
		TextureImporter texImporter;
		do {
			TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
			textureImporter.textureType = TextureImporterType.Sprite;
			textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
			textureImporter.spriteImportMode = SpriteImportMode.Multiple;
			textureImporter.mipmapEnabled = false;
			
			SpriteMetaData[] sheetData;
			do {
				List<SpriteMetaData> mData = new List<SpriteMetaData>();
				Dictionary<string,TextureData> T = getTextureDataBlockFromTextureInfo( textureInfo);
				Dictionary<string, SpriteMetaData> P = getTextureSpriteBlockFromTextureDataBlock(T, atlas);
				foreach (KeyValuePair<string, SpriteMetaData> e in P) {
					mData.Add(e.Value);
				}
				sheetData = mData.ToArray();
			} while (false);
			
			textureImporter.spritesheet = sheetData;
			textureImporter.maxTextureSize = 4096;//TODO: 这个4096用起来有说法
			texImporter = textureImporter;
		} while(false);
		
		TextureImporterSettings tis = new TextureImporterSettings();
		texImporter.ReadTextureSettings(tis);
		tis.spriteMeshType = SpriteMeshType.FullRect;
		texImporter.SetTextureSettings(tis);
		AssetDatabase.ImportAsset(path);
	}
	
	/// <summary>
	/// 从图集信息装载到字典
	/// 	key: 图快的名字
	/// 	value: 图快信息
	/// 	作用:用来裁剪精灵图块等-转换精灵
	/// </summary>
	/// <returns>The texture data block from texture info.</returns>
	/// <param name="info">Info.</param>
	static Dictionary<string,TextureData> getTextureDataBlockFromTextureInfo (Dictionary<string, object>  info)
	{
		Dictionary<string,TextureData> m_blocks = new Dictionary<string, TextureData>();
		foreach (Dictionary<String, System.Object> textureRawData in 
		         info [ConstValues.A_SUBTEXTURE] as List<System.Object>) {
			TextureData textureData = new TextureData();
			textureData.Name = textureRawData [ConstValues.A_NAME].ToString();	
			textureData.X = float.Parse(textureRawData [ConstValues.A_X].ToString());
			textureData.Y = float.Parse(textureRawData [ConstValues.A_Y].ToString());
			textureData.Width = float.Parse(textureRawData [ConstValues.A_WIDTH].ToString());
			textureData.Height = float.Parse(textureRawData [ConstValues.A_HEIGHT].ToString());
			
			m_blocks.Add(textureData.Name, textureData);
		}
		return m_blocks;
	}
	
	/// <summary>
	/// 从图快信息转换对应的精灵Sheet信息
	/// 	key name
	/// 	value SpriteMetaData数据
	/// </summary>
	/// 
	static Dictionary<string, SpriteMetaData> getTextureSpriteBlockFromTextureDataBlock(
		Dictionary<string,TextureData> textureBlock,
		TextureAtlas atlas)
	{
		Dictionary<string, SpriteMetaData> spriteBlock = new Dictionary<string, SpriteMetaData>();
		
		float maxSize = atlas.Texture.width > atlas.Texture.height ? atlas.Texture.width : atlas.Texture.height;
		foreach (KeyValuePair<string,TextureData> e in textureBlock) {
			SpriteMetaData sprite = getMetaDataFrom(e.Key.Replace('/', '@'),
			                                        e.Value,
			                                        atlas.Texture.height,
			                                        maxSize);
			spriteBlock.Add(sprite.name, sprite);
		}
		return spriteBlock;
	}
	
	/// <summary>
	/// 从图集裁剪Sprite,初始锚点为左上角
	/// 	为什么取左上角？是一个很矛盾的问题
	/// </summary>
	/// <returns>The meta data from.</returns>
	/// <param name="name">Name.</param>
	/// <param name="textureData">Texture data.</param>
	/// <param name="textureHeight">Texture height.</param>
	static SpriteMetaData getMetaDataFrom(
		string name,
		TextureData textureData,
		float textureHeight,
		float maxTextureSize)
	{		
		SpriteMetaData sprite = new SpriteMetaData();
		sprite.name = name;
		sprite.border = new Vector4(0, 0, 0, 0);
		float px = textureData.X;
		float py = textureHeight - textureData.Y - textureData.Height;
		px = Mathf.Clamp(px, 0, maxTextureSize);
		py = Mathf.Clamp(py, 0, maxTextureSize);
		sprite.rect = new Rect(px, 
		                       py,
		                       textureData.Width, 
		                       textureData.Height);
		sprite.alignment = 1;
		return sprite;
	}


	/// ==============================================================================
	/// ==============================================================================
	/// 			Unity骨骼树
	/// 			只构建一个层次的骨骼,后面再重构组成像龙骨的骨骼树

	/// <summary>
	/// 构造节点树
	/// </summary>
	public static Dictionary<string, Transform> BuildBoneTree(GameObject root, 
	                                            ArmatureData data,
	                                            Dictionary<string, Sprite> _listSprite)
	{
		Dictionary<string, Transform> nodes = new Dictionary<string, Transform>();		
		List<Transform> mTemp = new List<Transform>();
		GameObject g = root;
		getDictionaryNode(g.transform, mTemp);
		
		Dictionary<string, BoneData> mlistBoneData = new Dictionary<string, BoneData>();
		foreach (BoneData boneData in data.BoneDataList) {
			mlistBoneData.Add(boneData.Name, boneData);
			
			GameObject bone = getNode(mTemp, boneData.Name);
			if (bone == null) {
				bone = new GameObject(boneData.Name);
			}
			nodes.Add(bone.name, bone.transform);
			
			if (boneData.Parent != null) {
				bone.transform.parent = g.transform;//_listComb[boneData.Parent];
			} else {
				bone.transform.parent = g.transform;
			}
			
			Transform boneTransform = bone.transform;
			DBTransform trans = boneData.Global;//boneData.Transform;
			boneTransform.localPosition = new Vector3(trans.X / 100, -trans.Y / 100, 0);
			boneTransform.localScale = new Vector3(trans.ScaleX, trans.ScaleY, 1);
			boneTransform.localRotation = Quaternion.Euler(0, 0, -trans.Rotation * Mathf.Rad2Deg);			
			//JudgeReversalBefore(bone, _trans);
		}
		
		foreach (Transform t in mTemp) {
			t.parent = null;
			GameObject.DestroyImmediate(t.gameObject);
		}
		
		return nodes;
	}

	/// <summary>
	/// 获取t的孩子节点到列表mTemp
	/// </summary>
	/// <param name="t">T.</param>
	/// <param name="mTemp">M temp.</param>
	static void getDictionaryNode(Transform t, List<Transform> mTemp)
	{
		int i = t.childCount;
		while (i-- > 0) {
			getDictionaryNode(t.GetChild(i), mTemp);
		}
		if (t.parent != null)
			mTemp.Add(t);
	}


	/// <summary>
	/// 从List中查找节点
	/// 	如果存在，从列表删除
	/// </summary>
	/// <returns>The node.</returns>
	/// <param name="T">T.</param>
	/// <param name="name">Name.</param>
	static GameObject getNode(List<Transform> T, string name)
	{
		Transform t = null;
		foreach (Transform e in T) {
			if (e.name == name) {
				t = e;
				break;
			}
		}
		if (t != null) {
			T.Remove(t);
			return t.gameObject;
		}
		return null;
	}

	/// ==============================================================================
	/// ==============================================================================
	/// 			Unity骨骼树安装Slot与Slot添加Sprite
	/// 


	/// <summary>
	/// 骨骼树添加Slot
	/// 	Slot添加SpriteRender组件
	/// </summary>
	public static Dictionary<string,SlotData> BuildBoneSprite(ArmatureData data,
	                                                          Dictionary<string, Transform> listComb,
	                                                          Dictionary<string, Sprite> listSprite,
	                                                          Dictionary<string, Vector2> mPovit)
	{
		Dictionary<string,SlotData> listSlotData = new Dictionary<string,SlotData>();
		foreach (SkinData skinData in data.SkinDataList) {
			foreach (SlotData slotData in skinData.SlotDataList) {
				listSlotData.Add(slotData.Name, slotData);
				Transform tp = listComb [slotData.Parent];
				//排序
				Vector3 mDepthPos = tp.position;
				mDepthPos.z = -slotData.ZOrder * 0.001f;
				tp.position = mDepthPos;
				
				SpriteRenderer render = buildSlotSprite(tp.gameObject, slotData.Name);
				if (slotData.DisplayDataList != null && slotData.DisplayDataList.Count > 0) {
					DisplayData displayData = slotData.DisplayDataList [0];
					string key = CCArmatureIO .getSpriteName(data.Name, displayData.Name, slotData.Name);
					if (listSprite.ContainsKey(key)) {
						render.sprite = listSprite [key];
					}
					
					key = data.Name + slotData.Name + displayData.Name;
					key = key.Replace('/', '@');
					if (mPovit.ContainsKey(key))
						render.transform.localPosition += (Vector3)(mPovit [key]) * 0.01f;
				}
			}
		}
		return listSlotData;
	}

	/// <summary>
	/// slot 添加SpriteRender
	/// </summary>
	/// <returns>The slot sprite.</returns>
	/// <param name="g">The green component.</param>
	/// <param name="slotname">Slotname.</param>
	static SpriteRenderer buildSlotSprite(GameObject g, string slotname)
	{
		string childname = "slot@" + slotname;
		Transform child = g.transform.FindChild(childname);
		if (child == null) {
			GameObject n = new GameObject(childname);
			n.transform.parent = g.transform;
			g = n;
		} else {
			g = child.gameObject;
		}
		g.transform.localPosition = Vector3.zero;
		g.transform.localRotation = Quaternion.identity;
		g.transform.localScale = Vector3.one;
		
		SpriteRenderer render = g.GetComponent <SpriteRenderer>();
		if (render == null) {
			render = g.AddComponent<SpriteRenderer>();
		}
		return render;
	}

	/// ==============================================================================
	/// ==============================================================================
	/// 			动画状态机的添加
	/// 


	public static List<State> BuildStateMachine(GameObject g,
	                                            out AnimatorControllerLayer layer)
	{		
		Animator anim = g.GetComponent <Animator>();
		if (anim == null) {
			anim = g.AddComponent <Animator>();
		}
		
		AnimatorController animatorController;
		string controlName =  CCArmatureIO.getAnimatorPath(g.name);
		if (File.Exists(controlName)) {
			Debug.Log("已经存在了 >= " + controlName);
			animatorController = AssetDatabase.LoadAssetAtPath(controlName,typeof(AnimatorController))as AnimatorController;
		} else {
			animatorController = AnimatorController.CreateAnimatorControllerAtPath(controlName);
		}
		anim.runtimeAnimatorController = animatorController;
		layer = animatorController.GetLayer(0);
		List<State> mState = new List<State>();
		int count = animatorController.layerCount;
		while (count-- > 0) {
			UnityEditorInternal.StateMachine sm = animatorController.GetLayer(count).stateMachine;
			int m = sm.stateCount;
			while (m-- > 0) {
				mState.Add(sm.GetState(m));
			}
		}
		return mState;
	}

	/// ==============================================================================
	/// ==============================================================================
	/// 			动画剪辑的添加
	
	public static AnimationClip BuildAnimationClipAndState(string mClipName,
	                                                       string stateName,
	                                                       List<State> mStates,
	                                                       AnimatorControllerLayer layer)
	{
		UnityEditorInternal.StateMachine sm = layer.stateMachine;		
		//string mClipName = BoneTreeRoot.gameObject.name + "@" + animationDataName;
		string mClipPath = CCArmatureIO.getAnimClipPath(mClipName);
		if (!File.Exists(mClipPath))
			CCArmatureIO.CreateAnimationClipForAnimator(mClipPath);		
		AnimationClip newClip = AssetDatabase.LoadAssetAtPath(mClipPath, typeof(AnimationClip)) as AnimationClip;
		List <State> mSaveClip = getSaveState(mStates, mClipName);
		if (mSaveClip.Count == 0) {
			State state = sm.AddState(stateName);
			state.SetAnimationClip(newClip, layer);
		}
		return newClip;
	}


	/// <summary>
	/// 从所有状态中查看有没有这个状态
	/// 状态机里面的状态不用前缀
	/// </summary>
	/// <returns>The save state.</returns>
	/// <param name="mStates">M states.</param>
	/// <param name="mClipName">M clip name.</param>
	static List<State> getSaveState(List<State> mStates, string mClipName)
	{
		List <State> mSaveClip = new List<State>();
		foreach (State e in mStates) {
			Motion mm = e.GetMotion();
			if (mm != null && mm.name.Equals(mClipName)) {
				mSaveClip.Add(e);
				Debug.Log(e.name + "  >>>  " + mClipName);
			}
		}
		return mSaveClip;
	}


	/// <summary>
	/// Adds the hide timeline.
	/// </summary>
	/// <param name="m_Clip">M_ clip.</param>
	/// <param name="ntime">Ntime.</param>
	/// <param name="mBoneTree">M bone tree.</param>
	/// <param name="animationData">Animation data.</param>
	/// <param name="armatureData">Armature data.</param>
	public static void AddHideTimeline(AnimationClip m_Clip,float ntime,Dictionary<string,Transform> mBoneTree,AnimationData animationData,
	                            ArmatureData armatureData)
	{
		//if (ntime < 0.0001f)
		ntime = 0.016f;
		List<string> mListHideName = AddHideTimeline(animationData, armatureData);
		foreach (string name in mListHideName) {
			string full_name = getPathFromTree(name, mBoneTree [name]);// + "/slot@" + name;
			setCurveBinding(m_Clip, full_name, 0, 0, ntime, 0);
		}
	}

	static List<string> AddHideTimeline(AnimationData animationData, ArmatureData armatureData)
	{
		List<string> m_HideName = new List<string>();
		List<BoneData> boneDataList = armatureData.BoneDataList;
		int i = boneDataList.Count;
		
		BoneData boneData;
		string boneName;
		while (i -- >0) {
			boneData = boneDataList [i];
			boneName = boneData.Name;
			TransformTimeline p = animationData.GetTimeline(boneName);
			if (p == null || p.Duration <= 0.00001f) { 
				m_HideName.Add(boneName);
			}
		}
		return m_HideName;
	}

	public static string getPathFromTree(string name, Transform t)
	{
		string path = name;
		do {
			if (t.parent == null || t.parent == t.root) {
				break;
			}		
			t = t.parent;
			name = t.name;
			path = name + "/" + path;
		} while (true);
		return path;
	}


	static void setCurveBinding(AnimationClip clip,string childName,float time1,float value1,float time2,float value2)
	{
		AnimationCurve constantCurve = new AnimationCurve();
		constantCurve.AddKey(time1, value1);
		constantCurve.AddKey(time2, value2);
		//_newClip.SetCurve(childName, typeof(SpriteRenderer), "m_Enabled", constantCurve);
		clip.SetCurve(childName, typeof(GameObject), "m_IsActive", constantCurve);
	}
}
