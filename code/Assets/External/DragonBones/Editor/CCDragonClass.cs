﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditorInternal;

using System;
using System.IO;
using DragonBones;
using DragonBones.Factorys;
using DragonBones.Animation;
using DragonBones.Objects;
using DragonBones.Display;
using DragonBones.Textures;
using DragonBones.Utils;
using System.Collections.Generic;
using Com.Viperstudio.Utils;
using Com.Viperstudio.Geom;

using CurveExtended;

public class FrameTransposition
{
	Keyframe[] newFrams_X ;
	Keyframe[] newFrams_Y ;
	Keyframe[] newFrams_Z ;
	Keyframe[] newFrams_RX;
	Keyframe[] newFrams_RY;
	Keyframe[] newFrams_RZ;
	Keyframe[] newFrams_RW;
	Keyframe[] newFrams_SX;
	Keyframe[] newFrams_SY;

	public FrameTransposition(int Count)
	{
		if (Count <= 0)
			return;
		newFrams_X = new Keyframe[Count];
		newFrams_Y = new Keyframe[Count];
		newFrams_Z = new Keyframe[Count];
		
		newFrams_RX = new Keyframe[Count];
		newFrams_RY = new Keyframe[Count];
		newFrams_RZ = new Keyframe[Count];
		newFrams_RW = new Keyframe[Count];
		
		newFrams_SX = new Keyframe[Count];
		newFrams_SY = new Keyframe[Count];
	}

	public void AddFrame(int i, float m_time, TransformFrame _tframe, float z, float Px=0, float Py=0)
	{
		DBTransform T = _tframe.Global;
		Quaternion R = Quaternion.Euler(0, 0, -T.Rotation * Mathf.Rad2Deg);
				
		newFrams_X [i] = new Keyframe(m_time, (T.X + Px) / 100);
		newFrams_Y [i] = new Keyframe(m_time, -(T.Y + Py) / 100);
		newFrams_Z [i] = new Keyframe(m_time, z);
		
		newFrams_RX [i] = new Keyframe(m_time, R.x);
		newFrams_RY [i] = new Keyframe(m_time, R.y);
		newFrams_RZ [i] = new Keyframe(m_time, R.z);
		newFrams_RW [i] = new Keyframe(m_time, R.w);
		
		newFrams_SX [i] = new Keyframe(m_time, T.ScaleX);
		newFrams_SY [i] = new Keyframe(m_time, T.ScaleY);
	}

	public void PushTo(AnimationClip newClip, string Key, float m_time)
	{
		if (newFrams_X == null || newFrams_X.Length <= 0)
			return;
		if (m_time < 0.001f)
			m_time = 0.0167f;
		
		AnimationCurve one = new AnimationCurve(new Keyframe[] {
			new Keyframe(0, 1),
			new Keyframe(m_time, 1)
		});

		//AnimationCurve zore= new AnimationCurve(new Keyframe[]{new Keyframe(0,0),new Keyframe(m_time,0)});
		newClip.SetCurve(Key, typeof(Transform), "localPosition.z", new AnimationCurve(newFrams_Z));
		newClip.SetCurve(Key, typeof(Transform), "localPosition.x", new AnimationCurve(newFrams_X));
		newClip.SetCurve(Key, typeof(Transform), "localPosition.y", new AnimationCurve(newFrams_Y));
		
		newClip.SetCurve(Key, typeof(Transform), "localScale.x", new AnimationCurve(newFrams_SX));
		newClip.SetCurve(Key, typeof(Transform), "localScale.y", new AnimationCurve(newFrams_SY));
		newClip.SetCurve(Key, typeof(Transform), "localScale.z", one);
		
		AnimationCurve m_rotationX = new AnimationCurve(newFrams_RX);
		AnimationCurve m_rotationY = new AnimationCurve(newFrams_RY);
		AnimationCurve m_rotationZ = new AnimationCurve(newFrams_RZ);
		AnimationCurve m_rotationW = new AnimationCurve(newFrams_RW);
		
		newClip.SetCurve(Key, typeof(Transform), "localRotation.x", m_rotationX);
		newClip.SetCurve(Key, typeof(Transform), "localRotation.y", m_rotationY);
		newClip.SetCurve(Key, typeof(Transform), "localRotation.z", m_rotationZ);
		newClip.SetCurve(Key, typeof(Transform), "localRotation.w", m_rotationW);
	}
}

public class ColorBranchFrame
{
	string m_string;
	List <Keyframe> m_keyframe = new List<Keyframe>();

	public ColorBranchFrame(string mstring)
	{
		this.m_string = mstring;
	}

	public void AddKey(float time, float color)
	{
		//if (time > 0 && m_keyframe.Count == 0) {
		//	m_keyframe.Add (new Keyframe (0, 1));
		//}
		m_keyframe.Add(new Keyframe(time, color));//这个因子不用除100>=color/100.0f
	}

	public void SetObjectReferenceCurve(AnimationClip mClip, string childName)
	{
		if (!IsExist())
			return;
		mClip.SetCurve(childName, typeof(SpriteRenderer), m_string, 
		                new AnimationCurve(getFrames()));
	}

	bool IsExist()
	{
		return (m_keyframe.Count > 0);
	}

	Keyframe[] getFrames()
	{
		if (m_keyframe.Count > 1)
			return m_keyframe.ToArray();
		return new Keyframe[]{new Keyframe(0, 1),new Keyframe(0.0167f, 1)};
	}
}

/// <summary>
///  暂时看不到Flash那边不改变颜色,
///  只改变alpha
/// </summary>
public class ColorSpriteFrame
{
	ColorBranchFrame mR = new ColorBranchFrame("m_Color.r");
	ColorBranchFrame mG = new ColorBranchFrame("m_Color.g");
	ColorBranchFrame mB = new ColorBranchFrame("m_Color.b");
	ColorBranchFrame mA = new ColorBranchFrame("m_Color.a");
	
	public void AddKey(float time, ColorTransform color)
	{
		if (color == null)
			return;
		mA.AddKey(time, color.AlphaMultiplier);
		mR.AddKey(time, color.RedMultiplier);
		mG.AddKey(time, color.GreenMultiplier);
		mB.AddKey(time, color.BlueMultiplier);
	}

	public void SetObjectReferenceCurve(AnimationClip mClip, string childName)
	{
		mA.SetObjectReferenceCurve(mClip, childName);
		mR.SetObjectReferenceCurve(mClip, childName);
		mG.SetObjectReferenceCurve(mClip, childName);
		mB.SetObjectReferenceCurve(mClip, childName);
	}

	public ColorTransform getColor()
	{
		ColorTransform color = new ColorTransform();
		color.AlphaMultiplier = 1;
		color.RedMultiplier = 1;
		color.GreenMultiplier = 1;
		color.BlueMultiplier = 1;
		return color;
	}
}

public class HideSpriteFrame
{
	AnimationCurve constantCurve = new AnimationCurve();
	bool m_bool = false;

	public HideSpriteFrame(int t)
	{
	}

	public void AddKey(float m, int index, int displayIndex)
	{
		float value = 1;
		if (displayIndex < 0) {
			value = 0;
			m_bool = true;
		}
		//m = (int)(m*60.0f) * (1/60.0f);
		constantCurve.AddKey(KeyframeUtil.GetNew(m, value, TangentMode.Stepped));
	}

	public void SetObjectReferenceCurve(AnimationClip newClip, string childName)
	{
		if (!IsExist())
			return;
		//_newClip.SetCurve(childName, typeof(SpriteRenderer), "m_Enabled", constantCurve);
		newClip.SetCurve(childName, typeof(GameObject), "m_IsActive", constantCurve);
	}

	bool IsExist()
	{
		return m_bool;
	}
}

public class CollectSpriteFrame
{
	List<ObjectReferenceKeyframe> spriteKeys = new List<ObjectReferenceKeyframe>();
	Dictionary<string, Sprite> listSprite;
	Dictionary<string, Vector2> m_allPovit;
	List<DisplayData> listDiaplayData;
	string armatureName;
	string boneName;
	AnimationCurve mCurveX = new AnimationCurve();
	AnimationCurve mCurveY = new AnimationCurve();

	public CollectSpriteFrame(Dictionary<string, Sprite>  map_sprite,
	                           Dictionary<string, Vector2> m_allPovit,
	                           List<DisplayData> list_displaydata,
	                           string armatureName,
	                           string boneName)
	{
		this.listSprite = map_sprite;
		this.m_allPovit = m_allPovit;
		this.listDiaplayData = list_displaydata;
		this.armatureName = armatureName;
		this.boneName = boneName;
	}
	
	void addKeyframe(float time, UnityEngine.Object b)
	{
		ObjectReferenceKeyframe K = new ObjectReferenceKeyframe();
		K.time = time;
		K.value = b;
		spriteKeys.Add(K);
	}
	
	string getSpriteName(int index)
	{
		return CCArmatureIO.getSpriteName(
			armatureName, listDiaplayData [index].Name, boneName);
	}
	
	string getSpriteName(int index, int X, int Y)
	{
		return CCArmatureIO.getSpriteName(
			armatureName, listDiaplayData [index].Name, boneName, X, Y);
	}

	string getPovitName(int index)
	{
		return (armatureName + boneName + listDiaplayData [index].Name).Replace('/', '@');
	}

	/// <summary>
	/// 20141104:: index > 0 是一个bug，如果出现 01 01 01的序列帧就看不到0的帧了
	/// </summary>
	/// <param name="time">Time.</param>
	/// <param name="index">Index.</param>
	public void AddKey(float time, int index)
	{
		if (index >= 0) {
			if (spriteKeys.Count == 0 && time > 0.01f) {
				addKeyframe(0, listSprite [getSpriteName(0)]);
				if (m_allPovit.ContainsKey(getPovitName(0))) {
					mCurveX.AddKey(KeyframeUtil.GetNew(0, m_allPovit [getPovitName(0)].x * 0.01f, TangentMode.Stepped));
					mCurveY.AddKey(KeyframeUtil.GetNew(0, m_allPovit [getPovitName(0)].y * 0.01f, TangentMode.Stepped));
				}
			}
			addKeyframe(time, listSprite [getSpriteName(index)]);
			if (m_allPovit.ContainsKey(getPovitName(index))) {
				mCurveX.AddKey(KeyframeUtil.GetNew(time, m_allPovit [getPovitName(index)].x * 0.01f, TangentMode.Stepped));
				mCurveY.AddKey(KeyframeUtil.GetNew(time, m_allPovit [getPovitName(index)].y * 0.01f, TangentMode.Stepped));
			}
		}
	}
	
	public bool IsSpriteFrameExist()
	{
		return spriteKeys.Count > 0;
	}
	
	public ObjectReferenceKeyframe[] getFrames()
	{
		return spriteKeys.ToArray();
	}
	
	public void SetObjectReferenceCurve(AnimationClip _newClip, string childName)
	{
		if (IsSpriteFrameExist()) {
			EditorCurveBinding bind = EditorCurveBinding.PPtrCurve(childName, typeof(SpriteRenderer), "m_Sprite");
			AnimationUtility.SetObjectReferenceCurve(_newClip, bind, getFrames());

			_newClip.SetCurve(childName, typeof(Transform), "localPosition.x", mCurveX);
			_newClip.SetCurve(childName, typeof(Transform), "localPosition.y", mCurveY);
		}
	}
}
