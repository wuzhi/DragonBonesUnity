﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditorInternal;

using System;
using System.IO;

public class CCAramtureImport : EditorWindow {

	[@MenuItem("DragonBones/打开导入窗口")]
	private static void Init()
	{
		CCAramtureImport window = (CCAramtureImport)GetWindow(typeof(CCAramtureImport), true, "数据转换");
		window.Show();
	}
	
	private void OnGUI()
	{
		if (GUILayout.Button("选取目录转换")) {
			(new CCArmatureData ()).DragonBoneToUnity ();
		}
		//if (GUILayout.Button("删除动画数据")) {
		//(new CCArmatureData ()).DeleteArmatureData();
		//}
		if (GUILayout.Button("裁剪精灵数据")) {
			(new CCDragonSprite ()).Exec ();
		}
		//if (GUILayout.Button("查看动画数据")) { 
		//	Debug.Log ("查看动画数据");
		//(new CCArmatureData ()).LoopArmatureData ();
		//}
	}
}
