﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditorInternal;

using System;
using System.IO;
using DragonBones;
using DragonBones.Factorys;
using DragonBones.Animation;
using DragonBones.Objects;
using DragonBones.Display;
using DragonBones.Textures;
using DragonBones.Utils;
using System.Collections.Generic;
using Com.Viperstudio.Utils;
using Com.Viperstudio.Geom;

/// <summary>
/// 解析转换龙骨数据的基类主要用来做帮助
/// </summary>
public class CCArmatureDataBase
{
	/// <summary>
	/// 内部异常类
	/// 	用来中断操作
	/// </summary>
	public class SCException : Exception
	{
		public SCException(string Message)
			: base(Message)
		{
		}
	}
	
	/// <summary>
	/// 异常中断
	/// </summary>
	/// <param name="s">S.</param>
	public static void MSG(string s)
	{
		throw new SCException(s);
	}
	
	/// <summary>
	/// 断言
	/// </summary>
	/// <param name="_bool">If set to <c>true</c> _bool.</param>
	public static void MSG(bool mBool)
	{
		if (!mBool)
			throw new SCException("NULL exception");
	}
	
	/// <summary>
	/// 判断选取的目录是否符合要求
	/// </summary>
	/// <returns>The select path.</returns>
	public static string getSelectPath()
	{
		UnityEngine.Object select = Selection.activeObject;
		
		if (select == null) {
			MSG("没有选取目录");
		}
		
		string path = AssetDatabase.GetAssetPath(select);
		if (path == "") {
			path = "Assets";
		} else if (Path.GetExtension(path) != "") {
			path = path.Replace(Path.GetFileName(path), "");
		}
		if (path [path.Length - 1] != '/') {
			path += "/";
		}
		Debug.Log("Select: >= " + path);
		IsCorrectPathFromDragonBonePath(path);
		
		return path;
	}
	
	// <summary>
	///  是否正确的龙骨数据路径
	/// </summary>
	static bool IsCorrectPathFromDragonBonePath(string dir)
	{
		if (File.Exists(dir + "/skeleton.json") && 
		    File.Exists(dir + "/texture.json") && 
		    (File.Exists(dir + "/texture.png") || Directory.Exists(dir + "/texture/"))) {
			return true;
		}
		MSG("选取目录不是龙骨数据 >= " + dir);
		return false;
	}
	
	/// <summary>
	/// 解析图集
	/// </summary>
	/// <returns>The texture atla from.</returns>
	/// <param name="texturePath">Texture path.</param>
	/// <param name="textureJson">Texture json.</param>
	/// <param name="info">Info.</param>
	static protected TextureAtlas getTextureAtlaDataFrom(string texturePath,
	                                                     string textureJson,
	                                                     ref Dictionary<String, System.Object> info)
	{
		Texture _textures = AssetDatabase.LoadAssetAtPath(texturePath, typeof(UnityEngine.Texture))as Texture;
		TextAsset jsonReader = AssetDatabase.LoadAssetAtPath(textureJson, typeof(TextAsset)) as TextAsset;
		if (jsonReader == null)
			Debug.LogError(textureJson + " >= texture.json LoadAsset NULL");
		TextReader reader = new StringReader(jsonReader.text);
		Dictionary<String, System.Object> atlasRawData = Json.Deserialize(reader) as Dictionary<String, System.Object>;
		AtlasData atlasData = AtlasDataParser.ParseAtlasData(atlasRawData);
		TextureAtlas textureAtlas = new TextureAtlas(_textures, atlasData, false);
		
		info = atlasRawData;
		
		return textureAtlas;
	}
	
	/// <summary>
	/// 解析骨骼Json文件
	/// </summary>
	/// <returns>The skeleton data.</returns>
	/// <param name="skeletonJson">Skeleton json.</param>
	static protected SkeletonData getSkeletonDataFrom(string skeletonJson)
	{
		TextAsset jsonReader = AssetDatabase.LoadAssetAtPath(
			skeletonJson, typeof(TextAsset)) as TextAsset;
		if (jsonReader == null)
			Debug.LogError(skeletonJson + " >= skeleton.json LoadAsset NULL");
		TextReader reader = new StringReader(jsonReader.text);
		Dictionary<String, System.Object> skeletonRawData = Json.Deserialize(reader) as Dictionary<String, System.Object>;
		SkeletonData skeletonData = ObjectDataParser.ParseSkeletonData(skeletonRawData);
		
		return skeletonData;
	}
}